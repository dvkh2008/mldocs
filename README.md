[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/dvkh2008%2Fmldocs/master)
.. image:: https://mybinder.org/badge_logo.svg
 :target: https://mybinder.org/v2/gl/dvkh2008%2Fmldocs/master
 
 # How to run

  1. Install Docker CE ([Install])
  2. Pull image - `docker pull dvkh2008/datascience:latest`
  3. Run image - `docker run -d -p 6006:6006 -p 8888:8888 -e PASSWORD=ntmachinelearning -v $(pwd):/~/app/ dvkh2008/datascience:latest`
  4. Open your browser and goto localhost:8888 and use the password `ntmachinelearning` to login to the jupyter notebook
  5. Not map folder`docker run -d -p 6006:6006 -p 8888:8888 -e PASSWORD=ntmachinelearning dvkh2008/datascience:latest`
  
 
 ### sessions
 1. network
 2. softmax regression
 3. linear regression
 4. KNN
 5. k means clustering
 6. gradient descent
 7. logistic regression
 8. multiple perceptron
 9. decision tree
 10. naive bayes classifier
 11. content based filtering
 