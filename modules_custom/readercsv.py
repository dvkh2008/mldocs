import pandas
import sys
import numpy as np
from sklearn.model_selection import train_test_split

def test():
    print ("readercsv realy :).")
    return

def reader(filePath):
    df = pandas.read_csv(filePath)
    return df

def shuffler(df):
  # return the pandas dataframe
  return df.reindex(np.random.permutation(df.index))

def slipt(data, percentage):
    _percentage = percentage/100;
    train = data[:int(len(data)*_percentage)]
    test = data[len(train):]
    return train,test

def split_train_and_test(filePath, label_name, percentage):
    _percentage = percentage/100;
    data = pandas.read_csv(filePath)
    data.head()
    y = data[label_name]
    X = data.drop([label_name],axis=1)
    x_train,x_test,y_train,y_test=train_test_split(X,y,test_size=_percentage)
    return x_train.values,x_test.values,y_train.values,y_test.values